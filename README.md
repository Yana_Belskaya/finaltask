# Final Project #

this is the final task, done by Yana Belskaya

### What this Final Project about? ###

* folder "task1" is for tests for the [online store](http://automationpractice.com/index.php).
* folder "task2" is for API tests fro [this site](https://gorest.co.in/public-api/users/).
* the "task3" folder contains a script for creating a docker container with a database, and filling this database.

### Tasks description ###

* task1:
1. Testing cart page for adding thing and deleting.
2. Testing logging page for registration and login.
3. Testing main page for: open login page, open cart page, open contact us page and searching.

* task2
1. Testing get user.
2. Testing post user.
3. Testing get user by id.
4. Testing delete user by id.

* task3
1. Making mysql image.
2. Creating docker container.
3. Filling database.

And also there is a python file with some requests for checking just created database.

### How to run tests? ###

tests are written using pytest, so you can run this with the command "pytest filenale.py"
