from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


class BasePage:

    general_url = "http://automationpractice.com/index.php"

    def open(self):
        self.browser.get(self.url)

    def __init__(self, browser, url):
        self.browser = browser
        self.url = url
        self.browser.implicitly_wait(5)

    def wait_element_clickable(self, find_by, locator, timeout=15):
        button = WebDriverWait(self.browser, timeout).until(
            EC.element_to_be_clickable((find_by, locator))
        )
        return button

    def wait_element(self, find_by, locator, timeout=15):
        button = WebDriverWait(self.browser, timeout).until(
            EC.presence_of_element_located((find_by, locator))
        )
        return button
