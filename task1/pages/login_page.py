from selenium.common.exceptions import NoSuchElementException
from task1.pages.base_page import BasePage
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import Select
import json


class LoginPageLocators:
    LOCATOR_EMAIL_FIELD = (By.CSS_SELECTOR, "#email_create")
    LOCATOR_CREATE_ACCOUNT = (By.CSS_SELECTOR, "#SubmitCreate > span")


class CreateAccountLocators:
    LOCATOR_FIRST_NAME_FIELD = (By.CSS_SELECTOR, "#customer_firstname")
    LOCATOR_SECOND_NAME_FIELD = (By.CSS_SELECTOR, "#customer_lastname")
    LOCATOR_PASSWORD_FIELD = (By.CSS_SELECTOR, "#passwd")
    LOCATOR_ADDRESS_FIRST_NAME = (By.CSS_SELECTOR, "#firstname")
    LOCATOR_ADDRESS_LAST_NAME = (By.CSS_SELECTOR, "#lastname")
    LOCATOR_ADDRESS = (By.CSS_SELECTOR, "#address1")
    LOCATOR_ADDRESS_CITY = (By.CSS_SELECTOR, "#city")
    LOCATOR_ADDRESS_STATE = (By.CSS_SELECTOR, "#id_state")
    LOCATOR_ADDRESS_ZIP_CODE = (By.CSS_SELECTOR, "#postcode")
    LOCATOR_MOBILE_PHONE = (By.CSS_SELECTOR, "#phone_mobile")
    LOCATOR_ASSIGN = (By.CSS_SELECTOR, "#alias")
    LOCATOR_REGISTER_BUTTON = (By.CSS_SELECTOR, "#submitAccount > span")
    LOCATOR_REGISTER_BLOCK = (By.CSS_SELECTOR, "#noSlide")
    LOCATOR_MY_ACCOUNT_TEXT = (By.CSS_SELECTOR, "#center_column>h1")


class ReLogLocators:
    LOCATOR_SIGN_OUT_BUTTON = (By.CSS_SELECTOR, "[title='Log me out']")
    LOCATOR_LOG_IN_FORM = (By.CSS_SELECTOR, "#login_form")
    LOCATOR_EMAIL_ADDRESS_FIELD = (By.CSS_SELECTOR, "#email")
    LOCATOR_PASSWORD_FIELD = (By.CSS_SELECTOR, "#passwd")
    LOCATOR_SIGN_IN_BUTTON = (By.CSS_SELECTOR, "#SubmitLogin")
    LOCATOR_MY_ACCOUNT_NAME = (By.CSS_SELECTOR, "[title='View my customer account']")


class LoginPage(BasePage):
    url_login_page = f"{BasePage.general_url}?controller=authentication&back=my-account"

    def should_be_login_page(self):
        self.should_be_login_url()
        assert self.should_be_login_form() is True, "something wrong with login page"
        assert self.should_be_register_form() is True, "something wrong with login page"

    def should_be_login_url(self):
        # реализуйте проверку на корректный url адрес используя метод current_url
        current_url = self.browser.current_url
        assert current_url in LoginPage.url_login_page, "not write url for login page"

    def should_be_login_form(self):
        # реализуйте проверку, что есть форма логина
        try:
            self.browser.find_element_by_css_selector("#login_form > div")
        except NoSuchElementException:
            return False
        return True

    def should_be_register_form(self):
        # реализуйте проверку, что есть форма регистрации на странице
        try:
            self.browser.find_element_by_css_selector("#create-account_form")
        except NoSuchElementException:
            return False
        return True


class LoginPageMore(BasePage):

    authentication_url = f"{BasePage.general_url}?controller=authentication"
    url_login_page = f"{BasePage.general_url}?controller=authentication&back=my-account"
    create_account_url = f"{url_login_page}/#account-creation"
    my_account_url = f"{BasePage.general_url}?controller=my-account"

    with open("../body_data.json", "r") as f:
        user_data = json.load(f)

    def create_account_first_step(self):

        add_email = self.wait_element_clickable(*LoginPageLocators.LOCATOR_EMAIL_FIELD)
        add_email.send_keys(LoginPageMore.user_data['email'])
        create_account_button = self.wait_element_clickable(*LoginPageLocators.LOCATOR_CREATE_ACCOUNT)
        create_account_button.click()
        self.wait_element(*CreateAccountLocators.LOCATOR_REGISTER_BLOCK)

    def fill_register_form(self):

        first_name_field = self.wait_element_clickable(*CreateAccountLocators.LOCATOR_FIRST_NAME_FIELD)
        first_name_field.send_keys(LoginPageMore.user_data['customer_firstname'])

        second_name_field = self.wait_element_clickable(*CreateAccountLocators.LOCATOR_SECOND_NAME_FIELD)
        second_name_field.send_keys(LoginPageMore.user_data['customer_lastname'])

        password = self.wait_element_clickable(*CreateAccountLocators.LOCATOR_PASSWORD_FIELD)
        password.send_keys(LoginPageMore.user_data['passwd'])

        address_first_name = self.wait_element_clickable(*CreateAccountLocators.LOCATOR_ADDRESS_FIRST_NAME)
        address_first_name.send_keys(LoginPageMore.user_data['firstname'])

        address_second_name = self.wait_element_clickable(*CreateAccountLocators.LOCATOR_SECOND_NAME_FIELD)
        address_second_name.send_keys(LoginPageMore.user_data['Secondname'])

        address = self.wait_element_clickable(*CreateAccountLocators.LOCATOR_ADDRESS)
        address.send_keys(LoginPageMore.user_data['address1'])

        address_city = self.wait_element_clickable(*CreateAccountLocators.LOCATOR_ADDRESS_CITY)
        address_city.send_keys(LoginPageMore.user_data['city'])

        address_state_select = Select(self.wait_element(*CreateAccountLocators.LOCATOR_ADDRESS_STATE))
        address_state_select.select_by_value(LoginPageMore.user_data['id_state'])

        address_zip_code = self.wait_element_clickable(*CreateAccountLocators.LOCATOR_ADDRESS_ZIP_CODE)
        address_zip_code.send_keys(LoginPageMore.user_data['postcode'])

        mobile_phone = self.wait_element_clickable(*CreateAccountLocators.LOCATOR_MOBILE_PHONE)
        mobile_phone.send_keys(LoginPageMore.user_data['phone_mobile'])

        allias = self.wait_element_clickable(*CreateAccountLocators.LOCATOR_ASSIGN)
        allias.send_keys(LoginPageMore.user_data['alias'])

        register_button = self.wait_element_clickable(*CreateAccountLocators.LOCATOR_REGISTER_BUTTON)
        register_button.click()

        self.wait_element(*CreateAccountLocators.LOCATOR_MY_ACCOUNT_TEXT)
        current_url = self.browser.current_url
        assert current_url == LoginPageMore.my_account_url, "my account url isn't correct"

    def re_log_in_to_account(self):

        sign_out = self.wait_element_clickable(*ReLogLocators.LOCATOR_SIGN_OUT_BUTTON)
        sign_out.click()
        self.wait_element(*ReLogLocators.LOCATOR_LOG_IN_FORM)
        input_email = self.wait_element(*ReLogLocators.LOCATOR_EMAIL_ADDRESS_FIELD)
        input_email.send_keys(LoginPageMore.user_data['email'])
        input_password = self.wait_element(*ReLogLocators.LOCATOR_PASSWORD_FIELD)
        input_password.send_keys(LoginPageMore.user_data['passwd'])
        submit_button = self.wait_element_clickable(*ReLogLocators.LOCATOR_SIGN_IN_BUTTON)
        submit_button.click()
        self.wait_element(*CreateAccountLocators.LOCATOR_MY_ACCOUNT_TEXT)
        current_url = self.browser.current_url
        assert current_url == LoginPageMore.my_account_url, "my account url isn't correct"
        my_account_name = self.wait_element(*ReLogLocators.LOCATOR_MY_ACCOUNT_NAME).text
        expected_name = f'{LoginPageMore.user_data["customer_firstname"]} ' \
                        f'{LoginPageMore.user_data["customer_lastname"]}{LoginPageMore.user_data["Secondname"]}'
        assert expected_name == my_account_name, "Name, isn't correct. Probably wrong account"
