from selenium.common.exceptions import NoSuchElementException
from task1.pages.base_page import BasePage
from selenium.webdriver.common.by import By


class CartPageLocators:

    LOCATOR_FIRST_PRODUCT = (By.XPATH, "//*[@id='homefeatured']/li[1]/div/div[2]/h5/a")
    LOCATOR_ADD_TO_CART_BUTTON = (By.XPATH, "//*[@id='add_to_cart']/button")
    LOCATOR_PROCEED_TO_CHECKOUT = (By.XPATH, "//*[@id='layer_cart']/div[1]/div[2]/div[4]/a/span")
    LOCATOR_SUMMARY_PRODUCT = (By.CSS_SELECTOR, "#summary_products_quantity")
    LOCATOR_DELETE_STAFF_BUTTON = (By.CLASS_NAME, "cart_quantity_delete")
    LOCATOR_YOUR_CART_IS_EMPTY = (By.CSS_SELECTOR, "[class='ajax_cart_no_product']")


class CartPage(BasePage):

    url_cart_page = f"{BasePage.general_url}?controller=order"

    def should_be_cart_page(self):
        self.should_be_cart_url()
        assert self.should_be_empty_cart() is True, "something wrong with cart page"

    def should_be_cart_url(self):
        current_url = self.browser.current_url
        assert current_url in CartPage.url_cart_page, "not write url for cart page"

    def should_be_empty_cart(self):
        try:
            self.browser.find_element_by_css_selector("[id='emptyCartWarning']")
        except NoSuchElementException:
            return True
        return False


class CartPageMore(BasePage):

    def add_staff_to_cart(self, expected_summary_product="1 Product"):
        staff = self.wait_element(*CartPageLocators.LOCATOR_FIRST_PRODUCT)
        staff.click()
        add_to_cart_button = self.wait_element_clickable(*CartPageLocators.LOCATOR_ADD_TO_CART_BUTTON)
        add_to_cart_button.click()
        proceed_to_checkout = self.wait_element_clickable(*CartPageLocators.LOCATOR_PROCEED_TO_CHECKOUT)
        proceed_to_checkout.click()
        text_summary_product = self.wait_element(*CartPageLocators.LOCATOR_SUMMARY_PRODUCT).text
        assert text_summary_product == expected_summary_product, "Not correct sum of products"

    def delete_staff_from_cart(self, expected_cart_text="(empty)"):
        delete_button = self.wait_element(*CartPageLocators.LOCATOR_DELETE_STAFF_BUTTON)
        delete_button.click()
        self.wait_element(*CartPageLocators.LOCATOR_YOUR_CART_IS_EMPTY)
        text_cart_is_empty = self.wait_element(*CartPageLocators.LOCATOR_YOUR_CART_IS_EMPTY)
        assert text_cart_is_empty.text == expected_cart_text, "Something wrong with cart text"
        print(text_cart_is_empty.text)
