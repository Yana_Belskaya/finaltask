from task1.pages.base_page import BasePage
from selenium.webdriver.common.by import By
from task1.pages.login_page import LoginPage
from task1.pages.cart_page import CartPage
from requests import get


class MainPageLocators:
    LOCATOR_LOGIN_BUTTON = (By.CLASS_NAME, "login")
    LOCATOR_CONTACT_US_BUTTON = (By.ID, "contact-link")
    LOCATOR_SHOPPING_CART = (By.CLASS_NAME, "shopping_cart")
    LOCATOR_SEARCH_FIELD = (By.CSS_SELECTOR, "[name='search_query']")
    LOCATOR_SEARCH_SUBMIT_BUTTON = (By.CSS_SELECTOR, "[name='submit_search']")
    LOCATOR_SEARCH_RESPONSE_TEXT = (By.CSS_SELECTOR, "[class='page-heading  product-listing']")


class MainPage(BasePage):

    contact_us_url = f"{BasePage.general_url}?controller=contact"

    def page_title(self):
        return self.browser.title.text

    def check_url_main_page(self):
        current_url = self.browser.current_url
        assert current_url in BasePage.general_url, "not write url for login page"

    def open_login_page(self):
        button_login_page = self.wait_element_clickable(*MainPageLocators.LOCATOR_LOGIN_BUTTON)
        button_login_page.click()
        return LoginPage(self.browser, self.browser.current_url)

    def open_cart_page(self):
        button_cart_page = self.wait_element_clickable(*MainPageLocators.LOCATOR_SHOPPING_CART)
        button_cart_page.click()
        return CartPage(self.browser, self.browser.current_url)

    def open_contact_us_page(self):
        button_contact_us_page = self.wait_element_clickable(*MainPageLocators.LOCATOR_CONTACT_US_BUTTON)
        button_contact_us_page.click()
        current_page_url = get(self.browser.current_url).url
        assert current_page_url == MainPage.contact_us_url, "not required page"

    def input_in_search_field(self, text):
        search_field = self.wait_element(*MainPageLocators.LOCATOR_SEARCH_FIELD)
        search_field.send_keys(text)
        search_submit_button = self.wait_element_clickable(*MainPageLocators.LOCATOR_SEARCH_SUBMIT_BUTTON)
        search_submit_button.click()
        response_search = self.wait_element(*MainPageLocators.LOCATOR_SEARCH_RESPONSE_TEXT).text
        expected = "results have been found"
        assert expected in response_search, "not expected response"
