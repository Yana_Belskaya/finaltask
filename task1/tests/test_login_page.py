from task1.pages.login_page import LoginPageMore


def test_create_account_first_step(open_browser):
    create_account_page = LoginPageMore(open_browser, LoginPageMore.url_login_page)
    create_account_page.open()
    create_account_page.create_account_first_step()
    create_account_page.fill_register_form()
    create_account_page.re_log_in_to_account()
