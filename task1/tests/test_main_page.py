from task1.pages.main_page import MainPage
from task1.pages.base_page import BasePage


def test_correct_main_page(open_browser):
    main_page = MainPage(open_browser, BasePage.general_url)
    main_page.open()
    main_page.check_url_main_page()


def test_user_can_open_login_page(open_browser):
    main_page = MainPage(open_browser, BasePage.general_url)
    main_page.open()
    login_page = main_page.open_login_page()
    login_page.should_be_login_page()


def test_user_can_open_cart_page(open_browser):
    main_page = MainPage(open_browser, BasePage.general_url)
    main_page.open()
    cart_page = main_page.open_cart_page()
    cart_page.should_be_cart_page()


def test_user_can_open_contact_us_page(open_browser):
    main_page = MainPage(open_browser, BasePage.general_url)
    main_page.open()
    main_page.open_contact_us_page()


def test_user_can_search(open_browser):
    main_page = MainPage(open_browser, BasePage.general_url)
    main_page.open()
    main_page.input_in_search_field("Dress")
