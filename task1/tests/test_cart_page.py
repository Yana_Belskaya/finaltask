from task1.pages.cart_page import CartPageMore
from task1.pages.base_page import BasePage


def test_add_staff_to_cart(open_browser):
    add_staff = CartPageMore(open_browser, BasePage.general_url)
    add_staff.open()
    add_staff.add_staff_to_cart()
    add_staff.delete_staff_from_cart()
