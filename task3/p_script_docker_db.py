import docker
import mysql.connector as mysql
import json
import time


def create_container():
    client = docker.from_env()
    client.containers.run("mysql:8.0", detach=True, environment=["MYSQL_ROOT_PASSWORD=qwerty123"],
                          volumes={"/Users/yanabelskaya/PycharmProjects/finaltask/task3/docker": {
                              'bind': '/var/lib/mysql'}, "/Users/yanabelskaya/PycharmProjects/finaltask/task3/config": {
                              'bind': '/etc/mysql/conf.d'}
                          }, ports={'3306/tcp': 3307})
    print("container created")
    time.sleep(5)
    create_db()
    create_tables()
    container_id = str(client.containers.list())[13:23]
    with open('docker_id.json', 'w') as f:
        json.dump(container_id, f)
    container = client.containers.get(container_id)
    container.stop()


def create_db():
    db = mysql.connect(host="127.0.0.1",
                       port=3307,
                       user="root",
                       passwd="qwerty123")
    cursor = db.cursor()
    cursor.execute("CREATE DATABASE carsharing")
    print("database created")


def create_tables():
    db = mysql.connect(host="127.0.0.1",
                       port=3307,
                       user="root",
                       passwd="qwerty123",
                       database="carsharing")

    cursor = db.cursor()

    def get_info_for_table(this_info):
        with open("transport_data.json", 'r') as f:
            data = json.load(f)[this_info]
        return data

    # создание таблицы транспорт
    cursor.execute("""CREATE TABLE transport (car_type VARCHAR(255), mark VARCHAR(255), description VARCHAR(255),
    state_number VARCHAR(255) PRIMARY KEY, rental_prices VARCHAR(255), transmission_type VARCHAR(255),
    mileage VARCHAR(255), drive VARCHAR(255))""")

    sql = """insert into transport(car_type, mark, description, state_number, rental_prices, transmission_type,
    mileage, drive) values (%s, %s, %s, %s, %s, %s, %s, %s)"""
    value = get_info_for_table("transport")
    values = (value["car_type"], value["mark"], value["description"], value["state_number"], value["rental_prices"],
              value["transmission_type"], value["mileage"], value["drive"])
    cursor.execute(sql, values)
    db.commit()

    # создание таблицы покупатели
    cursor.execute("""CREATE TABLE buyers (buyer_id VARCHAR(255) PRIMARY KEY, name VARCHAR(255), phone_number VARCHAR(255),
    address VARCHAR(255))""")
    sql = """insert into buyers(buyer_id, name, phone_number, address) values (%s, %s, %s, %s)"""

    value = get_info_for_table("buyers")
    values = (value["buyer_id"], value["name"], value["phone_number"], value["address"])
    cursor.execute(sql, values)
    db.commit()

    # создание таблицы истории аренды
    cursor.execute("""CREATE TABLE rental_history (rental_id VARCHAR(255), buyer_id VARCHAR(255),
    state_number VARCHAR(255), date_rental_start VARCHAR(255), date_rental_end VARCHAR(255), 
    FOREIGN KEY(buyer_id) REFERENCES buyers(buyer_id), FOREIGN KEY(state_number) REFERENCES transport(state_number))""")
    sql = """insert into rental_history(rental_id, buyer_id, state_number, date_rental_start, date_rental_end)
    values (%s, %s, %s, %s, %s)"""

    value = get_info_for_table("rental_history")
    values = (value["rental_id"], value["buyer_id"], value["state_number"],
              value["date_rental_start"], value["date_rental_end"])
    cursor.execute(sql, values)
    db.commit()

    print("tables created")

    db.close()


if __name__ == '__main__':
    create_container()
