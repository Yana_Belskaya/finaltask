import mysql.connector as mysql
import json
import docker


with open('docker_id.json', 'r') as f:
    docker_id = json.load(f)
client = docker.from_env()
container = client.containers.get(docker_id)
container.start()


db = mysql.connect(host="127.0.0.1",
                   port=3307,
                   user="root",
                   passwd="qwerty123",
                   database="carsharing")

cursor = db.cursor()
print(db)


def get_cars_n_state_number():
    query = "SELECT mark, state_number FROM transport"
    cursor.execute(query)
    print(cursor.fetchall())


def get_cars_n_state_number_no_nissan():
    query = "SELECT mark, state_number FROM transport WHERE mark != 'Nissan'"
    cursor.execute(query)
    print(cursor.fetchall())


def get_cars_n_state_number_100_150_k():
    query = "SELECT mark, state_number, mileage FROM transport WHERE 150 < mileage < 100"
    cursor.execute(query)
    print(cursor.fetchall())


def add_couple_cars():
    query = """INSERT INTO transport (car_type, mark, description, state_number, rental_prices, transmission_type, 
    mileage, drive) VALUES (%s, %s, %s, %s, %s, %s, %s, %s)"""
    values1 = ("Hatchback", "Nissan", " life shook her", 6473, 50, "automatic", 160, "all-wheel")
    values2 = ("Hatchback", "Honda", " New car", 3744, 150, "automatic", 20, "all-wheel")
    cursor.execute(query, values1)
    cursor.execute(query, values2)
    db.commit()


def delete_cars():
    query1 = "DELETE FROM transport WHERE mark = 'Nissan'"
    query2 = "DELETE FROM transport WHERE mark = 'Honda'"
    cursor.execute(query1)
    cursor.execute(query2)
    db.commit()


def get_num_of_clients():
    query = "SELECT COUNT(1) FROM buyers"
    cursor.execute(query)
    print(cursor.fetchall())


def add_discount_in_table_buyers():
    query = "ALTER TABLE buyers ADD COLUMN discount VARCHAR(255)"
    cursor.execute(query)
    db.commit()


def get_max_discount():
    query = "SELECT MAX(discount) FROM buyers"
    cursor.execute(query)
    print(cursor.fetchall())


def get_common_mileage():
    query = "SELECT SUM(mileage) FROM transport"
    cursor.execute(query)
    print(cursor.fetchall())


get_cars_n_state_number()
get_cars_n_state_number_no_nissan()
get_cars_n_state_number_100_150_k()
# add_couple_cars()
# get_cars_n_state_number()
# delete_cars()
# get_cars_n_state_number()
get_num_of_clients()
# add_discount_in_table_buyers()
get_max_discount()
get_common_mileage()
