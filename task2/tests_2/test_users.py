from task2.resourses_for_tests import user_requests


def test_get_user():
    response = user_requests.get_users()
    assert response is not None
    assert response.json()["code"] == 200, "Couldn't get a list of users"


def test_post_new_user():
    response = user_requests.post_user()
    assert response.json()["code"] == 201, "Couldn't create a user"


def test_get_user_by_id():
    response = user_requests.get_user_by_id()
    name = user_requests.pull_user_info()
    assert name == response.json()['data']['name'], "The username does not match"


def test_negative_create_again_user():
    response = user_requests.post_user()
    assert response.json()["code"] == 422, "Negative create user isn't work. Something went not by the plan"
