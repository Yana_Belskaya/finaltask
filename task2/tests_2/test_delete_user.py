from task2.resourses_for_tests import user_requests


def test_post_new_user():
    response = user_requests.post_user()
    assert response.status_code == 200, "can't create new user"


def test_delete_user():
    response = user_requests.delete_user_by_id()
    print(response.json())
    assert response.json()["code"] == 204, "the user is not deleted"


def test_negative_delete_user():
    response = user_requests.delete_user_by_id()
    print(response.json())
    assert response.json()["code"] == 404, "User still exist or something else went wrong"
