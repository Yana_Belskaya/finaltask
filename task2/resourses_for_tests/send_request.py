import requests


def send_request(method, url, headers=None, params=None, body=None, expected_status=200):
    response = requests.request(method=method, url=url, headers=headers, params=params, json=body)
    assert response.status_code == expected_status, "something went wrong, status code isn't 200"
    return response
