import json
from task2.resourses_for_tests.send_request import send_request


general_url = "https://gorest.co.in/public-api/users/"


def get_users():
    response = send_request("get", general_url)
    return response


def post_user():
    with open('../info_for_tests.json', 'r') as f:
        body = json.load(f)
    with open('../info_for_tests.json', 'r') as f:
        header = json.load(f)
    response = send_request("post", general_url, headers=header[1], body=body[0])

    # записываю текущую инфу о юзере
    with open('../current_user_info.json', 'w') as f:
        info = response.json()
        json.dump(info, f)

    return response


def get_user_by_id():
    with open('../current_user_info.json', 'r') as f:
        user_id = json.load(f)['data']['id']
    response = send_request("get", f"{general_url}{int(user_id)}")
    return response


def delete_user_by_id():
    with open('../current_user_info.json', 'r') as f:
        user_id = json.load(f)['data']['id']
    with open('../info_for_tests.json', 'r') as f:
        header = json.load(f)
    response = send_request("delete", f"{general_url}{int(user_id)}", headers=header[1])
    return response


"""  getting user info from json file   """
def pull_user_info(pull_this='name'):
    with open('../info_for_tests.json', 'r') as info:
        user_data = json.load(info)[0][pull_this]
    return user_data
